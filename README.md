## OnePlus7TPro_EEA-user 10 QKQ1.190716.003 1910120055 release-keys
- Manufacturer: oneplus
- Platform: msmnile
- Codename: OnePlus7TPro
- Brand: OnePlus
- Flavor: carbon_hotdog-user
- Release Version: 11
- Id: RQ3A.210705.001
- Incremental: 4
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: OnePlus/OnePlus7TPro_EEA/OnePlus7TPro:10/QKQ1.190716.003/1910120055:user/release-keys
- OTA version: 
- Branch: OnePlus7TPro_EEA-user-10-QKQ1.190716.003-1910120055-release-keys
- Repo: oneplus_oneplus7tpro_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
